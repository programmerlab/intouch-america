<?php /* Template Name: BYOD Detail Page Template */ ?>

<?php get_header(); ?>

<?php 
$referrer = $_SERVER['HTTP_REFERER'];

// BYOD Details Page
$prevUrl = get_permalink(7);

if($referrer != $prevUrl) {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
}
?>

<div class="sub-banner">
    <img src="<?php bloginfo('template_directory'); ?>/img/sub-banner.jpg" width="100%;">
</div>

<div class="plan-process">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <div id="steps">
                    <ul>
                        <li><div class="step" data-desc="Plans">1</div></li>
                        <li><div class="step active" data-desc="Phones">2</div></li>
                        <li><div class="step" data-desc="Checkout">3</div></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="due-box">
                    <span>
                        Due Today

                        <?php
                        global $woocommerce;
                        //  id of targeted category is 5 for example

                        $price = 0;

                        // start of the loop that fetches the cart items

                        foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
                            $_product = $values['data'];
                            $_product_qty = $values['quantity'];
                            $price_new = get_post_meta($values['product_id'], '_price', true);

                            $terms = get_the_terms($_product->id, 'product_cat');

                            // second level loop search, in case some items have several categories
                            foreach ($terms as $term) {
                                $_categoryid = $term->term_id;
                                if ($_categoryid === 19) {

                                    $price = $price + $price_new;
                                }
                            }
                        }

                        echo '$ ' . $price;
                        ?>

                    </span>
                </div>

                <div class="due-box2">
                    <span>
                        Due Monthly

                        <?php
                        global $woocommerce;
                        //  id of targeted category is 5 for example

                        $price = 0;

                        // start of the loop that fetches the cart items

                        foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
                            $_product = $values['data'];
                            $_product_qty = $values['quantity'];
                            $price_new = get_post_meta($values['product_id'], '_price', true);

                            $terms = get_the_terms($_product->id, 'product_cat');

                            // second level loop search, in case some items have several categories
                            foreach ($terms as $term) {
                                $_categoryid = $term->term_id;
                                if ($_categoryid === 35) {

                                    $price = $price + $price_new;
                                }
                            }
                        }

                        echo '$ ' . $price;
                        ?>

                    </span>
                </div>
                <div class="add-to-cart-btn">
                    <button class="cart-toggle"><i class="fa fa-sort-desc" aria-hidden="true"></i> View Cart <i class="fa fa-sort-desc" aria-hidden="true"></i></button>
                </div>
                <div class="show-toggle" style="display:none">
                    <div class="head-cart">
                        <h3>VIEW CART ITEMS</h3>
                    </div>
                    <div class="cart-detail">

                        <?php
                        global $woocommerce;
                        $cart = $woocommerce->cart->cart_contents;

                        foreach ($cart as $item) {
                            ?>
                            <div id="<?php echo 'view-cart-' . $item['product_id']; ?>">   
                                <div class="cart-left">
                                    <h4>Product Name</h4>
                                </div>
                                <div class="cart-right">
                                    <h4><?php echo $item['data']->post->post_title; ?></h4>
                                </div>      
                                <div class="cart-left">
                                    <h4>Quantity</h4>
                                </div>
                                <div class="cart-right">
                                    <h4><?php echo $item['quantity']; ?></h4>
                                </div>  
                                <div class="cart-left">
                                    <h4>Price</h4>
                                </div>
                                <div class="cart-right">
                                    <h4>$<?php echo $item['line_total']; ?></h4>
                                </div>  
                                <div class="cart-left">
                                    <h4></h4>
                                </div>
                                <div class="cart-right">
                                    <button type="button" onClick="removeCart(<?php echo $item['product_id']; ?>)" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                    <span style="border-bottom: 1px solid #ccc;"></span>
                                </div>  
                            </div> 
                        <?php } ?>
                        <div class="cart-left">
                            <h4>Cart Total</h4>
                        </div>
                        <div class="cart-right">
                            <h4><?php echo $woocommerce->cart->get_cart_total(); ?></h4>
                        </div>  
                    </div>
                    <div class="head-cart">
                        <h3>INVOICE CREDITS Next Invoice</h3>
                    </div>
                    <div class="cart-detail">
                        <p><span>First Month Free</span> Your order qualifies for our First Month Free promotion! Talk and Connect Plans, plus any additional line fees, will be complimentary for your first monthly billing cycle. We'll apply this credit to your first invoice.</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
        	<div class="planDtlPg clearfix">
            	<div class="simImg">
                	<div class="simImgInr">
                        <?php 
                            $pageID = get_the_ID();
                            $img = wp_get_attachment_image_src( get_post_thumbnail_id( $pageID ), 'full' );
                         ?>
                    	<img src="<?php  echo $img[0] ?>" alt="Sim Image" />
                    </div>
                </div>
                <div class="plnRght">
                	<div class="prHead">
                    	<a href="">Back to My Phones</a>
                        <h1>Intouch America SIM Card</h1>
                    </div>    
                    <div class="prSngl">
                        <?php $pageID = get_the_ID(); ?>
                        <h3><?= get_field('question_1', $pageID); ?></h3>
                        <div class="checkBtns">
                            <div class="cbSngl">
                                <label for="question_1_option_1">
                                    <input type="radio" name="question_1_option" id="question_1_option_1" value="1" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span><?= get_field('question_1_option_1', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                            <div class="cbSngl">
                                <label for="question_1_option_2">
                                    <input type="radio" name="question_1_option" id="question_1_option_2" value="2" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span><?= get_field('question_1_option_2', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                            <div class="cbSngl">
                                <label for="question_1_option_3">
                                    <input type="radio" name="question_1_option" id="question_1_option_3" value="3" checked="checked" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span><?= get_field('question_1_option_3', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="prSngl">    
                        <h3><?= get_field('question_2', $pageID); ?></h3>
                        <div class="checkBtns">
                            <div class="cbSngl">
                                <label for="question_2_option_1">
                                    <input type="radio" name="question_2_option" id="question_2_option_1" value="1" checked="checked" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span><?= get_field('question_2_option_1', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                            <div class="cbSngl">
                                <label for="question_2_option_2">
                                    <input type="radio" name="question_2_option" id="question_2_option_2" value="2" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span><?= get_field('question_2_option_2', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="prSngl">    
                        <h3><?= get_field('question_3', $pageID); ?></h3>
                        <div class="checkBtns">
                            <div class="cbSngl epDtl">
                                <label for="question_3_option_1">
                                    <input type="radio" name="question_3_option" id="question_3_option_1" value="1" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span class="question_3_option_1"><?= get_field('question_3_option_1', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                            <div class="cbSngl">
                                <label for="question_3_option_2">
                                    <input type="radio" name="question_3_option" id="question_3_option_2" value="2" checked="checked" />
                                    <div class="cbCnt">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span class="question_3_option_2"><?= get_field('question_3_option_2', $pageID); ?></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="enDtls clearfix" style="display: none;">
                        	<div class="endFld inptFld">
                            	<label>Phone Number</label>
                                <input type="text" placeholder="Phone Number" id="phone_number" />
                                <span class="error" id="phone_number_error"></span>
                            </div>
                            <div class="endFld inptFld">
                            	<label>Account Holder Full Name</label>
                                <input type="text" placeholder="Full Name" id="full_name" />
                                <span class="error" id="full_name_error"></span>
                            </div>
                            <div class="endFld inptFld">
                            	<label>Account Number</label>
                                <input type="text" placeholder="Account Number" id="account_number" />
                                <span class="error" id="account_number_error"></span>
                            </div>
                            <div class="endFld inptFld">
                            	<label>Account Password</label>
                                <input type="text" placeholder="Account Password" id="account_password" />
                                <span class="error" id="account_password_error"></span>
                            </div>
                            <div class="endFld txtArea">
                            	<label>Account Number Billing Address</label>
                                <textarea rows="4" placeholder="Account Number Billing Address" id="billing_address"></textarea>
                                <span class="error" id="billing_address_error"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="nextBtn clearfix">
                    	<a href="javascript:void(0);" class="licence_btn btnNextStep">CONTINUE TO NEXT STEP</a>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>


<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.cookie.js"></script>
<script type="text/javascript">
    jQuery(function($) {
        $('#question_3_option_1').click(function() {
            //alert('Show Me');
            $('.enDtls').show();
        });

        $('#question_3_option_2').click(function() {
            //alert('Hide Me');
            $('.enDtls').hide();
        });

        $('.btnNextStep').on('click', function() {
            var question_1_answer = '';
            var question_2_answer = '';
            var question_3_answer = '';
            var phone_number = '';
            var full_name = '';
            var account_number = '';
            var account_password = '';
            var billing_address = '';
            $('#phone_number_error').text('');
            $('#full_name_error').text('');
            $('#account_number_error').text('');
            $('#account_password_error').text('');
            $('#billing_address_error').text('');


            $.each($("input[name='question_1_option']:checked"), function() {                
                question_1_answer = $(this).val();
            });
            $.each($("input[name='question_2_option']:checked"), function() {
                question_2_answer = $(this).val();
            });
            $.each($("input[name='question_3_option']:checked"), function() {
                question_3_answer = $(this).val();
            });

            var flag = 1;
            if(question_3_answer == 1) {
                if($('#phone_number').val() == '') {
                    $('#phone_number_error').text('Please enter Phone Number');
                    flag = 0;
                } else {
                    phone_number = $('#phone_number').val();
                }

                if($('#full_name').val() == '') {
                    $('#full_name_error').text('Please enter Full Name');
                    flag = 0;
                } else {
                    full_name = $('#full_name').val();
                }

                if($('#account_number').val() == '') {
                    $('#account_number_error').text('Please enter Account Number');
                    flag = 0;
                } else {
                    account_number = $('#account_number').val();
                }

                if($('#account_password').val() == '') {
                    $('#account_password_error').text('Please enter Account Password');
                    flag = 0;
                } else {
                    account_password = $('#account_password').val();
                }

                if($('#billing_address').val() == '') {
                    $('#billing_address_error').text('Please enter Billing Address');
                    flag = 0;
                } else {
                    billing_address = $('#billing_address').val();
                }
            }

            if(flag == 0) {
                return false;
            } else {
                //console.log(question_1_answer, question_2_answer, question_3_answer);
                //console.log(phone_number, full_name, account_number, account_password, billing_address);
                $.cookie("question_1_answer", question_1_answer);
                $.cookie("question_2_answer", question_2_answer);
                $.cookie("question_3_answer", question_3_answer);
                $.cookie("phone_number", phone_number);
                $.cookie("full_name", full_name);
                $.cookie("account_number", account_number);
                $.cookie("account_password", account_password);
                $.cookie("billing_address", billing_address);

                window.location.href = '/plans';
            }
        });
    });
</script>

<style type="text/css">
    span.error {color: red;}
</style>

<?php get_footer(); ?>
