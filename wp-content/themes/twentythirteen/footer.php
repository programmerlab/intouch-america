<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<div class="footer-main">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 block">
                    <h4>COMPANY INFO</h4>
                    <ul>
                        <li><a href="#">Cell Phones</a></li>
                        <li><a href="#">Smartphones</a></li>
                        <li><a href="#">Senior Friendly Phones</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Cell Phone Lingo</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 block">
                    <h4>IMPORTANT LINKS</h4>
                    <ul>
                        <li><a href="#">Cell Phones</a></li>
                        <li><a href="#">Smartphones</a></li>
                        <li><a href="#">Senior Friendly Phones</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Cell Phone Lingo</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 block">
                    <h4>PRODUCTS</h4>
                    <ul>
                        <li><a href="#">Cell Phones</a></li>
                        <li><a href="#">Smartphones</a></li>
                        <li><a href="#">Senior Friendly Phones</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Cell Phone Lingo</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 block">
                    <h4>CONNECT WITH US</h4>
                    <P class="call_us"><i class="fa fa-phone"></i> <span>(855) 563-6993</span></P>
                    <P class="call_us"><i class="fa fa-envelope"></i> <span>info@intouchamerica.com</span></P>
                    <br/>
                    <p><img src="<?php bloginfo('template_directory'); ?>/img/foot-logo.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <!--footer end-->

    <div class="last_footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <span class="pull-right">© 2017 Intouch America , Inc. All Rights Reserved</span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://use.fontawesome.com/a832a5b49f.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>

<script>
    var checkedVal = jQuery("input[name='keep_your_existing_phone_number']:checked").val();

    if (checkedVal == 'no') {

        jQuery(".wccpf-fields-group-3").hide();
        jQuery(".wccpf-fields-group-4").hide();
        jQuery(".wccpf-fields-group-5").hide();
        jQuery(".wccpf-fields-group-6").hide();
        jQuery(".wccpf-fields-group-7").hide();
    }

    jQuery("input[name='keep_your_existing_phone_number']").change(function () {

        if (jQuery(this).val() == "yes")
        {

            jQuery(".wccpf-fields-group-3").show();
            jQuery(".wccpf-fields-group-4").show();
            jQuery(".wccpf-fields-group-5").show();
            jQuery(".wccpf-fields-group-6").show();
            jQuery(".wccpf-fields-group-7").show();

        } else {

            jQuery(".wccpf-fields-group-3").hide();
            jQuery(".wccpf-fields-group-4").hide();
            jQuery(".wccpf-fields-group-5").hide();
            jQuery(".wccpf-fields-group-6").hide();
            jQuery(".wccpf-fields-group-7").hide();
        }
    });

    jQuery(function () {
        jQuery('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                    $button = $widget.find('button'),
                    $checkbox = $widget.find('input:checkbox'),
                    color = $button.data('color'),
                    settings = {
                        on: {
                            icon: 'glyphicon glyphicon-check'
                        },
                        off: {
                            icon: 'glyphicon glyphicon-unchecked'
                        }
                    };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                        .removeClass()
                        .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                            .removeClass('btn-default')
                            .addClass('btn-' + color + ' active');
                } else {
                    $button
                            .removeClass('btn-' + color + ' active')
                            .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });


    jQuery('.toBottom').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                || location.hostname == this.hostname) {

            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });


    jQuery(document).ready(function () {
        jQuery('#nav_fix').affix({
            offset: {
                top: 100
            }
        });
    });

    /* highlight the top nav as scrolling occurs */
    jQuery('body').scrollspy({target: '#nav_fix'})





</script>
<script>
    jQuery(".contact_button").click(function () {
        jQuery(".contact_menu").toggle();
    });
</script>
<script>
    jQuery(".menu_button").click(function () {
        jQuery(".menu_menu").toggle();
    });

    jQuery(".cart-toggle").click(function () {
        jQuery(".show-toggle").toggle();
    });


    $('.step').each(function (index, el) {
        $(el).not('.active').addClass('done');
        $('.done').html('<i class="icon-valid"></i>');
        if ($(this).is('.active')) {
            $(this).parent().addClass('pulse')
            return false;
        }
    });



//jQuery('#lineItem').prop("disabled",true);

    /*****************************************************************************/

    if (jQuery("#lineItem").val() == '229') {

        jQuery("#line-cart").val('0.0');
    } else {

        jQuery("#line-cart").val('10.00');
    }

    jQuery("#simple-cart").val('0');
    jQuery("#smartphone-cart").val('0');

    var simple_price = jQuery("#simple-cart").val();
    var smart_price = jQuery("#smartphone-cart").val();
    var lineCart = jQuery("#line-cart").val();

    var check = 0;
    var checktwo = 0;

    function myFunction(id, proid, price, that) {
        var currentObject = jQuery(that).parents('.owl-item');
        var url = document.location;
        jQuery("#simple-cart").val(price);
        simple_price = jQuery("#simple-cart").val();
        //jQuery('.a').removeClass('active-price');
        //jQuery('#box' + proid).addClass('active-price');
        jQuery('#simple_pro').val(proid);
        total = parseFloat(simple_price) + parseFloat(smart_price) + parseFloat(lineCart);
        total = total.toFixed(2);
        jQuery("#total-cart").val(total);
        jQuery("#plan-price").html("$" + total);
        //jQuery('#box' + proid).parents('.owl-stage').find('.owl-item').removeClass('active');
        //currentObject.parents('.slider-1').find('p.price-pos-add').remove();
        jQuery('.owl-carousel.slider-1 .owl-item').removeClass('current-selected active center');
        currentObject.addClass('current-selected active center');
        
        currentObject.parents('.slider-1').find('.price-pos').hide();
        jQuery(that).parents('.new-price-box').find('.price-pos').show();
        
        var owlNumber = currentObject.index();
        jQuery('.owl-carousel.slider-1').trigger('to.owl.carousel', owlNumber);
    }

    function mySecFunction(id, proid, price, that) {
        var currentObject = jQuery(that).parents('.owl-item');
        var url = document.location;
        jQuery("#smartphone-cart").val(price);
        smart_price = jQuery("#smartphone-cart").val();
        //jQuery('.b').removeClass('active-price');
        //jQuery('#box2' + proid).addClass('active-price');
        jQuery('#smart_pro').val(proid);
        total = parseFloat(simple_price) + parseFloat(smart_price) + parseFloat(lineCart);
        total = total.toFixed(2);
        jQuery("#total-cart").val(total);
        jQuery("#plan-price").html("$" + total);

        //currentObject.parents('.slider-2').find('p.price-pos-add').remove();
        jQuery('.owl-carousel.slider-2 .owl-item').removeClass('current-selected active center');
        currentObject.addClass('current-selected active center');

        currentObject.parents('.slider-2').find('.price-pos').hide();
        jQuery(that).parents('.new-price-box').find('.price-pos').show();
        
        var owlNumber = currentObject.index();
        jQuery('.owl-carousel.slider-2').trigger('to.owl.carousel', owlNumber);
    }


    /**************plan page show hide div**************************************************************/

    jQuery(function () {


        $('#check_plan_val').prop('checked', false);
        jQuery("#check_plan_val").click(function () {
            if (jQuery(this).is(":checked")) {


                jQuery("#smartphone-cart").val('0');
                jQuery("#smart-phone-div").hide();
                jQuery('#smart_pro').val('');

                simple_price = jQuery("#simple-cart").val();

                simple_price = parseFloat(simple_price) + parseFloat(lineCart);

                jQuery("#total-cart").val(simple_price);

                jQuery("#plan-price").html("$" + simple_price);



            } else {
                jQuery("#smart-phone-div").show();
                jQuery('.b').removeClass('active-price');
            }
        });
    });




    jQuery('#lineItem').on('change', function () {

        var line = this.value;

        if (line == '230') {
            jQuery("#line-cart").val('10.00');

            simple_price = jQuery("#simple-cart").val();
            smart_price = jQuery("#smartphone-cart").val();
            lineCart = jQuery("#line-cart").val();

            total = parseFloat(simple_price) + parseFloat(smart_price) + parseFloat(lineCart);

            total = total.toFixed(2);

            jQuery("#total-cart").val(total);

            jQuery("#plan-price").html("$" + total);

        } else {

            jQuery("#line-cart").val('0.00');

            simple_price = jQuery("#simple-cart").val();
            smart_price = jQuery("#smartphone-cart").val();
            lineCart = jQuery("#line-cart").val();

            total = parseFloat(simple_price) + parseFloat(smart_price) + parseFloat(lineCart);

            total = total.toFixed(2);

            jQuery("#total-cart").val(total);

            jQuery("#plan-price").html("$" + total);
        }


    });

    /*****************************************************************************/

    function addTocart() {

        var url = document.location;

        var sPlan = jQuery('#simple_pro').val();
        var spPlan = jQuery('#smart_pro').val();
        var line_item = $('select[name="line"] option:selected').val();


        if (sPlan != '') {

            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

            if (spPlan != '') {

                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl,
                    data: {'simplepro_id': sPlan, 'smartpro_id': spPlan, 'line_item': line_item},
                    success: function (res) {
                        if (res) {

                            window.location = url + '/phones';
                        }
                    }
                });

            } else {

                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl,
                    data: {'simplepro_id': sPlan, 'line_item': line_item},
                    success: function (res) {
                        if (res) {

                            window.location = url + '/phones';
                        }
                    }
                });
            }

        } else {

            alert('Please select Monthly Plan');
            return false;
        }

    }


    function checkAddTocart() {

        var url = document.location;

        var sPlan = jQuery('#simple_pro').val();
        var spPlan = jQuery('#smart_pro').val();
        var line_item = $('select[name="line"] option:selected').val();
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

        if(sPlan == '') {
            alert('Please select Monthly Plan');
            $('html, body').animate({
                scrollTop:$('.owl-carousel.slider-1').offset().top - 100
            }, 'slow');
            return false;
        } else {
            if(spPlan == '') {
                if(!jQuery('#check_plan_val').is(':checked')) {
                    alert('Please select Smartphone Plan');
                    $('html, body').animate({
                        scrollTop:$('.owl-carousel.slider-2').offset().top - 100
                    }, 'slow');
                    return false;
                } else {
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: {'simplepro_id': sPlan, 'line_item': line_item},
                        success: function (res) {
                            if (res) {
                                cart_add(265);
                                window.location = url + '/checkout';
                            }
                        }
                    });    
                }
            } else {
                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl,
                    data: {'simplepro_id': sPlan, 'smartpro_id': spPlan, 'line_item': line_item},
                    success: function (res) {
                        if (res) {
                            cart_add(265);
                            window.location = url + '/checkout';
                        }
                    }
                });
            }
        }
    }

    function cart_add(id, mytype = '') {

        var url = document.location.origin;

        jQuery.ajax({
            type: 'POST',
            url: url + '/cart/?add-to-cart=' + id,
            data: {'product_id': id},
            success: function (response, textStatus, jqXHR) {
                console.log("Product added");
                if (mytype == 'byod') {
                    window.location.href = '<?php echo get_permalink(268); ?>';
                }
            },
        });

        jQuery("#accessories-" + id).html('<a onclick="cart_remove(' + id + ')"><span class="glyphicon glyphicon-minus"></span></button>');

    }



    function cart_remove(id) {

        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: {action: 'remove_item_from_cart', 'product_id': id},
            success: function (res) {
                if (res) {
                    console.log('Removed Successfully');
                }
            }
        });

        jQuery("#accessories-" + id).html('<a onclick="cart_add(' + id + ')"><span class="glyphicon glyphicon-plus"></span></button>');

    }

    function removeCart(id) {

        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: {action: 'remove_item_from_cart', 'product_id': id},
            success: function (res) {
                if (res) {
                    jQuery("#view-cart-" + id).hide();
                    jQuery(".woocommerce-Price-amount").html('<span class="woocommerce-Price-currencySymbol">$</span>' + res + '</span>');
                }
            }
        });

    }

    /*
     function myRejSecFunction(id,proid,price){
     var url = document.location;
     
     var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
     
     jQuery.ajax({
     type: "POST",
     url: ajaxurl,
     data: {action : 'remove_item_from_cart','product_id' : proid},
     success: function (res) {
     if (res) {
     alert('Removed Successfully');
     }
     }
     }); 
     
     
     jQuery('#box2'+proid).removeClass('active-price');
     jQuery("#select-"+proid).html('<button id="demo" onClick="mySecFunction('+id+','+proid+','+price+')">Select Plan <i class="fa fa-check"></i></button>');
     
     }
     
     
     
     function myRejFunction(id,proid,price){
     
     var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
     
     jQuery.ajax({
     type: "POST",
     url: ajaxurl,
     data: {action : 'remove_item_from_cart','product_id' : proid},
     success: function (res) {
     if (res) {
     // alert('Removed Successfully');
     }
     }
     });
     
     // jQuery("#selectone-"+proid).html('<button id="demo" onClick="myFunction('+id+','+proid+','+price+')">Select Plan <i class="fa fa-plus"></i></button>');
     jQuery('#box'+proid).removeClass('active-price');
     
     cart_price = cart_price-price;
     
     var num = cart_price.toFixed(2);
     
     jQuery("#plan-price").html("$"+num);
     
     } 
     
     /*
     
     function mySecFunction(id,proid){
     var url = document.location;
     
     jQuery.ajax({
     type: 'POST',
     url: url+'?add-to-cart='+proid,
     data: { 'product_id':  id},
     success: function(response, textStatus, jqXHR){
     alert("Product added");
     console.log("Product added");
     
     },
     
     }); 
     
     //jQuery("#select-"+id).html('<button id="demo" onClick="mySecRejFunction('+id+','+proid+')">Reject Plan <i class="fa fa-check"></i></button>');
     jQuery('#box2'+id).addClass('active-price');
     
     } 
     function mySecRejFunction(id,proid){
     var url = document.location;
     
     var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
     
     jQuery.ajax({
     type: "POST",
     url: ajaxurl,
     data: {action : 'remove_item_from_cart','product_id' : proid},
     success: function (res) {
     if (res) {
     alert('Removed Successfully');
     }
     }
     });
     
     
     jQuery("#select-"+id).html('<button id="demo" onClick="mySecFunction('+id+','+proid+')">Select Plan <i class="fa fa-plus"></i></button>');
     jQuery('#box2'+id).removeClass('active-price');
     
     }
     */
</script>
<script>
    $('.new-pricing .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        loop: true,
        nav: true,
        center: true,
        responsiveClass: true,
        responsive: {
            0: {items: 1},
            400: {items: 2},
            580: {items: 3},
            700: {items: 4},
            990: {items: 5}
        }
    });
</script>
<script>
	$('.nav>li.menu-item-has-children').click(function(e) {
        $(this).siblings().removeClass('openMenu');
		$(this).siblings().children('.sub-menu').slideUp();
		$(this).toggleClass('openMenu');
		$(this).children('.sub-menu').slideToggle();
    });
</script>

<?php wp_footer(); ?>
</body>
</html>