<?php
/* Template Name: Coverage Map Page template */
get_header(); 
?>

<div class="sub-banner">
    <img src="<?php bloginfo('template_directory'); ?>/img/sub-banner.jpg" width="100%;">
</div>
   
<div class="choose-plan covrMap">
    <div class="container">
        <div class="row">            
            <div class="col-md-12">
            	<h2><?php the_title(); ?></h2>                    
            </div>
            <div class="col-sm-6">
                <div class="choose-plan-content">
                    <?php while ( have_posts() ) : the_post(); ?>                
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                    <div class="check-coverage">
                        <p>Enter your zip code to check network coverage in your area.</p>
                        <div class="input-coverage">
                            <form method="post" onsubmit="return false;">
                                <input class="zip-in" type="text" id="zipCode" placeholder="For e.g., 90210" value="" />
                                <input type="submit" value="Check Coverage" id="btnCoverageMap" />
                            </form>
                            <span class="error" id="zipCode_error"></span>
                        </div>
                        <!--<div class="coverage-map" id="map"></div>-->
                    </div>
                    <div class="scusMsg" id="successBox" style="display: none;">
                	   <i class="fa fa-check" aria-hidden="true"></i>
                        <div class="smInr">
                            <p><strong>Yes!</strong></p>
                            <p>Service is available in that area.</p>
                        </div>
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="mapImg">
                	<img src="<?php bloginfo('template_directory'); ?>/img/map4.jpg">
                </div>
            </div> 
        </div>
    </div>
</div>

<style type="text/css">
    span.error {color: red;font-weight: 600;font-size: 13px;}
    /*div#map {height: 300px;margin: 15px 0 0 0;}*/
</style>

<!-- <script language=javascript src='http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDCVA8DKd1ndEf63c_KGXnq05we4kmZSWs'></script> -->
<script type="text/javascript">
    jQuery(function($) {

        $('#btnCoverageMap').on('click', function() {
            $('#zipCode_error').text('');

            if($('#zipCode').val() == '') {
                $('#zipCode_error').text('Please enter US Zip Code');
            } else {
                var zipCode = $('#zipCode').val();
                var client = new XMLHttpRequest();
                client.open("GET", "http://api.zippopotam.us/us/" + zipCode, true);
                client.onreadystatechange = function() {
                    if(client.readyState == 4) {
                        if(client.responseText != '{}') {
                            $('#successBox').show();
                            //var response = JSON.parse(client.responseText);                            
                            //var lat = response.places[0].latitude;
                            //var lng = response.places[0].longitude;
                            //console.log(response, lat, lng);
                            //createMap(lat, lng);
                        } else {
                            $('#successBox').hide();                                  
                            $('#zipCode_error').text('Invalid US Zip Code');
                        }                        
                    };
                };
                client.send();
            }
        });

        // function createMap(lat, lng) {
        //     var myLatlng = new google.maps.LatLng(lat, lng);
        //     var mapOptions = {zoom: 4, center: myLatlng}
        //     var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        //     var marker = new google.maps.Marker({
        //         position: myLatlng,
        //         icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
        //         title:"Network is Available"
        //     });

        //     var contentString = '<div id="content">Network is Available</div>';
        //     var infowindow = new google.maps.InfoWindow({
        //         content: contentString
        //     });
            
        //     marker.setMap(map);
        //     marker.addListener('click', function() {
        //         infowindow.open(map, marker);
        //     });
        //     infowindow.open(map,marker);
        // }

        // var lat = 42.877742;
        // var lng = -97.380979;
        // createMap(lat, lng);
    });
</script>


<?php get_footer(); ?>