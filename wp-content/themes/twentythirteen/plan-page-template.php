<?php
/* Template Name: Plan Page template */
get_header();
?>

<?php 
$referrer = $_SERVER['HTTP_REFERER'];

// BYOD Details Page
$prevUrl = get_permalink(268);

if($referrer != $prevUrl) {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
}
?>

<div class="sub-banner">
    <img src="<?php bloginfo('template_directory'); ?>/img/sub-banner.jpg" width="100%;">
</div>

<div class="plan-process">
    <div class="container">
        <div class="row plan-shadow">
            <div class="col-sm-8">
                <div id="steps">
                    <ul>
                        <li><div class="step active" data-desc="Plans">1</div></li>
                        <li><div class="step" data-desc="Phones">2</div></li>
                        <li><div class="step" data-desc="Checkout">3</div></li>
                        <!-- <li><div class="step" data-desc="Validation">4</div></li>
                          <li><div class="step" data-desc="Payment">5</div></li>
                          <li><div class="step" data-desc="Resume">6</div></li>-->
                        <!--         <li><div class="step" data-desc="Other">7</div></li> -->
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="due-box">
                    <span>
                        Due Today
                        <?php
                        global $woocommerce;
                        //  id of targeted category is 5 for example

                        $price = 0;

                        // start of the loop that fetches the cart items

                        foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
                            $_product = $values['data'];
                            $_product_qty = $values['quantity'];
                            $price_new = get_post_meta($values['product_id'], '_price', true);

                            $terms = get_the_terms($_product->id, 'product_cat');

                            // second level loop search, in case some items have several categories
                            foreach ($terms as $term) {
                                $_categoryid = $term->term_id;
                                if ($_categoryid === 19) {

                                    $price = $price + $price_new;
                                }
                            }
                        }

                        echo '$ ' . $price;
                        ?>

                    </span>
                </div>

                <div class="due-box2">
                    <span>
                        Due Monthly

                        <?php
                        global $woocommerce;
                        //  id of targeted category is 5 for example

                        $price = 0;

                        // start of the loop that fetches the cart items

                        foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
                            $_product = $values['data'];
                            $_product_qty = $values['quantity'];
                            $price_new = get_post_meta($values['product_id'], '_price', true);

                            $terms = get_the_terms($_product->id, 'product_cat');

                            // second level loop search, in case some items have several categories
                            foreach ($terms as $term) {
                                $_categoryid = $term->term_id;
                                if ($_categoryid === 35) {

                                    $price = $price + $price_new;
                                }
                            }
                        }

                        echo '$ ' . $price;
                        ?>
                    </span>
                </div>
                <div class="add-to-cart-btn">
                    <button class="cart-toggle"><i class="fa fa-sort-desc" aria-hidden="true"></i> View Cart <i class="fa fa-sort-desc" aria-hidden="true"></i></button>
                </div>
                <div class="show-toggle" style="display:none">
                    <div class="head-cart">
                        <h3>VIEW CART ITEMS</h3>
                    </div>
                    <div class="cart-detail">

                        <?php
                        global $woocommerce;
                        $cart = $woocommerce->cart->cart_contents;

                        foreach ($cart as $item) {
                            ?>
                            <div id="<?php echo 'view-cart-' . $item['product_id']; ?>">   
                                <div class="cart-left">
                                    <h4>Product Name</h4>
                                </div>
                                <div class="cart-right">
                                    <h4><?php echo $item['data']->post->post_title; ?></h4>
                                </div>      
                                <div class="cart-left">
                                    <h4>Quantity</h4>
                                </div>
                                <div class="cart-right">
                                    <h4><?php echo $item['quantity']; ?></h4>
                                </div>  
                                <div class="cart-left">
                                    <h4>Price</h4>
                                </div>
                                <div class="cart-right">
                                    <?php if ($item['line_total'] == 0) { ?>
                                        <h4>FREE</h4>
                                    <?php } else { ?>
                                        <h4>$<?php echo $item['line_total']; ?></h4>
                                    <?php } ?>
                                </div>  
                                <div class="cart-left">
                                    <h4></h4>
                                </div>
                                <div class="cart-right">
                                    <button type="button" onClick="removeCart(<?php echo $item['product_id']; ?>)" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                    <span style="border-bottom: 1px solid #ccc;"></span>
                                </div>  
                            </div> 
                        <?php } ?>
                        <div class="cart-left">
                            <h4>Cart Total</h4>
                        </div>
                        <div class="cart-right">
                            <h4><?php echo $woocommerce->cart->get_cart_total(); ?></h4>
                        </div>  

                    </div>



                    <div class="head-cart">
                        <h3>INVOICE CREDITS
                            Next Invoice</h3>
                    </div>
                    <div class="cart-detail">
                        <p><span>First Month Free</span>
                            Your order qualifies for our First
                            Month Free promotion! Talk and
                            Connect Plans, plus any additional
                            line fees, will be complimentary for
                            your first monthly billing cycle. We'll
                            apply this credit to your first invoice.</p>
                    </div>


                </div>
            </div>
        </div>
    </div> 
</div>

<div class="rate-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                <span>
                    <label for="check_yes_im_vetern">
                        <input type="checkbox" id="check_yes_im_vetern" class="check-rate">
                        <span class="rate-content">YES I AM VETERN ADDITIONAL 5% OFF YOUR MONTHLY RATES FOR LIFE!</span> 
                        <img src="<?php bloginfo('template_directory'); ?>/img/flag.jpg" class="flag">
                    </label>                    
                </span>

            </div>
        </div>
    </div>
</div>

<div class="choose-plan">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="choose-plan-content">
                    <h2>Choose Your Best Plan Based On Your Needs</h2>
                    <h3>National Freedom Plans:</h3>
                    <p>At InTouch America we have a wide selection of cell phone plans to suit anyone. If you are unsure about choosing the best plan to fit your requirements and budget, please just give us call on (800) 500-0066 or send us a message Here. Our friendly 100% U.S. based customer service team is always happy to help with any questions that you may have. We can always evaluate your specific wireless usage and recommend a great plan that would work best for your needs. </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="right-main-content">
                    <h3>Same Great Networks up to 74% Less!</h3>
                    <p>With our flexible cell phone plans, you are always in control.  You can easily change your plan at anytime withno extra charges.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="new-pricing">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="owl-carousel slider-1">
                    <?php
                    $i = 1; // counter
                    $args = array('post_type' => 'product', 'posts_per_page' => 5, 'product_cat' => 'simple-plans');
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) : $loop->the_post();
                        global $product;
                        global $woocommerce;
                        foreach ($woocommerce->cart->get_cart() as $key => $val) {
                            $_product = $val['data'];
                            if ($product_id == $_product->id) {
                                return true;
                            }
                        }
                        ?>
                        <div class="item" id="<?php echo "box" . $product->id; ?>">    
                            <div class="new-price-box a">
                                <p class="main-price">$<?php echo $product->get_display_price($product->get_regular_price()) ?></p>
                                <p class="m-acc"><?php echo get_the_title() ?></p>
                                <p id="selectone-<?php echo $product->id; ?>" class="select-btn s_one">
                                    <button id="demo" onClick="myFunction(<?php echo $i; ?>,<?php echo $product->id; ?>,<?php echo $product->get_display_price($product->get_regular_price()) ?>, this)">
                                        Select Plan <i class="fa fa-plus"></i><i class="fa fa-check" style="display:none;"></i>
                                    </button>
                                </p>
                                <p><img src="<?php bloginfo('template_directory'); ?>/img/free-dark.png"></p>
                                <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>120 Minutes Included</span></p>
                                <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>$0.30 Additional Airtime</span></p>
                                <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>Free Long Distance</span></p>   
                                <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>Free Long Distance</span></p>
                                <p class="price-pos" style="display: none;"><img src="<?php bloginfo('template_directory'); ?>/img/price-pos.png"></p>
                                <p class="price-pos-add"><i class="glyphicon glyphicon-plus"></i></p>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="smart-value">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Smartphone value plans:</h2>
                <p>Exceptional Talk, Text & Data Plans on America's Best 4G Networks</p>
            </div>
        </div>
    </div>
</div>
<div class="rate-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                <span>
                    <label for="check_plan_val">
                        <input type="checkbox" id="check_plan_val" class="check-rate">
                        <span class="rate-content">I don't need a Smartphone Value Plan</span> 
                        <img src="<?php bloginfo('template_directory'); ?>/img/flag.jpg" class="flag">
                    </label>
                </span>
            </div>
        </div>
    </div>
</div>
<div id="smart-phone-div"> 
    <div class="new-pricing">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="owl-carousel slider-2">
                        <?php
                        $j = 1; // counter
                        $args = array('post_type' => 'product', 'posts_per_page' => 5, 'product_cat' => 'smartphone-value-plans');
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                            global $product;
                            ?>
                            <div class="item" id="<?php echo "box2" . $product->id; ?>">    
                                <div class="new-price-box b">
                                    <p class="main-price">$<?php echo $product->get_display_price($product->get_regular_price()) ?></p>
                                    <p class="m-acc"><?php echo get_the_title() ?></p>
                                    <p id="select-<?php echo $product->id; ?>" class="select-btn s_two">
                                        <button id="demo" onClick="mySecFunction(<?php echo $j; ?>,<?php echo $product->id; ?>,<?php echo $product->get_display_price($product->get_regular_price()) ?>, this)">
                                            Select Plan <i class="fa fa-plus"></i><i class="fa fa-check" style="display:none;"></i>
                                        </button>
                                    </p>
                                    <p><img src="<?php bloginfo('template_directory'); ?>/img/free-dark.png"></p>
                                    <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>120 Minutes Included</span></p>
                                    <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>$0.30 Additional Airtime</span></p>
                                    <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>Free Long Distance</span></p>   
                                    <p><span><img src="<?php bloginfo('template_directory'); ?>/img/right-ico.png"></span> <span>Free Long Distance</span></p>
                                    <p class="price-pos" style="display: none;"><img src="<?php bloginfo('template_directory'); ?>/img/price-pos.png"></p>
                                    <p class="price-pos-add"><i class="glyphicon glyphicon-plus"></i></p>
                                </div>
                            </div>
                            <?php
                            $j++;
                        endwhile;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container line-dropdown">
    <div class="row">
        <div class="col-sm-12">
            <select name="line" id="lineItem" class="form-control lines">
                <option value="230">Two Line</option>
                <option value="229" selected="selected">One Line</option>
            </select>
        </div>
    </div>
</div>

<div class="container">
    <div class="choose-plan">
        <div class="row">
            <div class="col-sm-4">
                <div class="choose-plan-content">
                    <h2>MONTHLY PLAN TOTALS</h2>
                    <h3>National Freedom Plans:</h3>
                    <p class="text-justify">At InTouch America we have a wide selection of cell phone plans to suit anyone. If you are unsure about choosing the best plan to fit your requirements and budget, please just give us call on (800) 500-0066 or send us a message Here. Our friendly 100% U.S. based customer service team is always happy to help with any questions that you may have. We can always evaluate your specific wireless usage and recommend a great plan that would work best for your needs. </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="right-main-content continue-shoping" >
                    <input type="hidden" id="total-cart" value="0">
                    <input type="hidden" id="simple-cart" value="0">
                    <input type="hidden" id="smartphone-cart" value="0">
                    <input type="hidden" id="line-cart" value="0">
                    <h2 class="text-center"><span id="plan-price">$0</span> <br/>MONTHLY BILL</h2>
                </div>
                <input type="hidden" id="simple_pro" value="">
                <input type="hidden" id="smart_pro" value="">
                <?php
                global $woocommerce;
                $cart = $woocommerce->cart->cart_contents;

                $flagbtn = 0;
                foreach ($cart as $item) {
                    if ($item['product_id'] == 265) {
                        $flagbtn = 1;
                    }
                }
                ?>

                <?php if ($flagbtn == 0) { ?>
                    <button id="shop" onClick="addTocart()" class="add-to-cart-btn">Continue to Phone</button>
                <?php } else { ?>
                    <button id="shop" onClick="checkAddTocart()" class="add-to-cart-btn">Go To Checkout</button>
                    <?php //echo '<a style="color: #fff;" href="' . $woocommerce->cart->get_checkout_url() . '" title="' . __('Go To Checkout') . '">' . __('Go To Checkout') . '</a>'; ?>
                <?php } ?>
            </div>
            <div class="col-sm-4">
                <div class="choose-plan-content">
                    <h2>MONTHLY PLAN TOTALS</h2>
                    <h3>National Freedom Plans:</h3>
                    <p class="text-justify">At InTouch America we have a wide selection of cell phone plans to suit anyone. If you are unsure about choosing the best plan to fit your requirements and budget, please just give us call on (800) 500-0066 or send us a message Here. Our friendly 100% U.S. based customer service team is always happy to help with any questions that you may have. We can always evaluate your specific wireless usage and recommend a great plan that would work best for your needs. </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="features feature2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Included Features on all plans</h2>
            </div>
        </div>
        <div class="row categories">
            <div class="col-sm-4">
                <div class="box text-center">
                    <img src="<?php bloginfo('template_directory'); ?>/img/icon1.png" alt="ACA Compliance">
                    <h3>FREE Cell Phone</h3>
                    <p>Across the nation. Backed with 30 years of experience our service is free and our process is absolutely simple</p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/down_icon.png" alt="more">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box text-center">
                    <img src="<?php bloginfo('template_directory'); ?>/img/icon2.png" alt="ACA Compliance">
                    <h3>FREE Shipping</h3>
                    <p>Across the nation. Backed with 30 years of experience our service is free and our process is absolutely simple</p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/down_icon.png" alt="more">
                </div>
            </div>
            <div class="col-sm-4 right_border">
                <div class="box text-center">
                    <img src="<?php bloginfo('template_directory'); ?>/img/icon3.png" alt="ACA Compliance">
                    <h3>Best National Wireless Networks</h3>
                    <p>Across the nation. Backed with 30 years of experience our service is free and our process is absolutely simple</p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/down_icon.png" alt="more">
                </div>
            </div>
        </div>

        <div class="row categories">
            <div class="col-sm-4 ">
                <div class="box text-center bottom_border ">
                    <img src="<?php bloginfo('template_directory'); ?>/img/icon4.png" alt="ACA Compliance">
                    <h3>FREE Activation ($36 value)</h3>
                    <p>Across the nation. Backed with 30 years of experience our service is free and our process is absolutely simple</p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/down_icon.png" alt="more">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box text-center bottom_border">
                    <img src="<?php bloginfo('template_directory'); ?>/img/icon5.png" alt="ACA Compliance">
                    <h3>Veteran Discount</h3>
                    <p>Across the nation. Backed with 30 years of experience our service is free and our process is absolutely simple</p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/down_icon.png" alt="more">
                </div>
            </div>
            <div class="col-sm-4 right_border">
                <div class="box text-center bottom_border">
                    <img src="<?php bloginfo('template_directory'); ?>/img/icon6.png" alt="ACA Compliance">
                    <h3>Refer a Friend &amp; Get $10 Each</h3>
                    <p>Across the nation. Backed with 30 years of experience our service is free and our process is absolutely simple</p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/down_icon.png" alt="more">
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .owl-carousel .owl-dots.disabled, .owl-carousel .owl-nav.disabled {display: block;}
    p.price-pos-add {background: #a92936;z-index: 999999;width: 40px;height: 40px;color: #fff;border-radius: 0px 0 0 5px;position: absolute;top: 0;right: 0;}
    p.price-pos-add i.glyphicon.glyphicon-plus {width: 100%;height: 100%;text-align: center;margin: 0;top: 0;padding: 0;vertical-align: middle;line-height: 36px;}
</style>

<script type="text/javascript">
    jQuery(function ($) {
        var prevUrl = document.referrer;
        var byodUrl = '<?php echo get_permalink(268); ?>';
        if (prevUrl == byodUrl) {
            $('.line-dropdown').hide();
        } else {
            $('.line-dropdown').show();
        }
    });
</script>

<?php get_footer(); ?>
   