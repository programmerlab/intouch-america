<?php /* Template Name: BYOD Page Template */ ?>

<?php get_header(); ?>

<?php
global $woocommerce;
$woocommerce->cart->empty_cart();
//foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
    //if ($values['product_id'] == 267) {
        //$woocommerce->cart->empty_cart();
    //}
//}
?>

<div class="main-slider">
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
          <div class="item active">
              <!-- <img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg" alt="..." width="100%"> -->
              <img src="<?php bloginfo('template_directory'); ?>/img/sub-banner.jpg" width="100%;">
          </div>
          <!-- <div class="item">
              <img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg" alt="..." width="100%">
          </div>
          <div class="item">
              <img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg" alt="..." width="100%">
          </div> -->
      </div>
      <!-- Controls -->
      <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> -->
  </div>
  <!-- <div class="banner_content">
      <h1>Refer friends  &amp; family  when they sign up you  both get $10 each! </h1>
      <p>Customize your own talk, text and data plan starting at just $4.99 a month! Free cell phone, shipping and activation. -
      </p>
      <div class="front-testimonial">
          <iframe width="357" height="238" src="https://www.youtube.com/embed/mb-A1NIiGwI" frameborder="0" allowfullscreen=""></iframe>
      </div>
  </div> -->
  <!-- <div class="over_slider">
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                  <p class="banner-lower-cnt">Best Pay As You Go <span class="plan-text">Plans</span>. <span class="pay-text">Pay</span> Only For What You Need &amp; USe <a href="#" class="get-q">GET QUOTE</a></p>
              </div>
          </div>
      </div>
  </div> -->
</div>

<!--New Added Content Start Ravendra -->
<section class="newSec">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <iframe width="100%" height="300" src="https://www.youtube.com/embed/mb-A1NIiGwI" frameborder="0" allowfullscreen=""></iframe>
            </div>
            <div class="col-md-6">
                <div class="rghtTop">
                    <h1>INTOUCH AMERICA <span>SIM CARD</span></h1>
                    <div class="rtngCnt">
                        <div class="rcStars">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <a href="">(Read Reviews)</a>
                    </div>
                    <p>
                        Now you can use your own phone with our FREE SIM card. It's quick and easy, and there are no activation fees. To be compatible, your phone should be a Intouch America phone, a phone previously used with AT&T or T-Mobile, or an unlocked GSM phone.
                    </p>
                    <h2>FREE</h2>
                   
                    <button onclick="cart_add(265, 'byod')" class="btn btn-primary">Add to cart </button>
                </div>
            </div>
        </div>
        <div class="tabCnt">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Details</a>
                </li>
                <li role="presentation">
                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Customer Reviews </a></li>
                <li role="presentation">
                    <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"> Videos &amp; Manuals</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="tpInr">
                        <h2>INTOUCH AMERICA SIM CARD</h2>
                        <p>
                            Now you can use your own phone with our FREE SIM card. It's quick and easy, and there are no activation fees. To be compatible, your phone should be a Intouch America phone, a phone previously used with AT&T or T-Mobile, or an unlocked GSM phone.
                        </p>
                        <h3>ADDITIONAL DETAILS FOR INTOUCH AMERICA SIM CARD</h3>
                        <h5>Key Features</h5>
                        <ul>
                            <li>Use your own phone with our FREE SIM card</li>
                            <li>No activation fees</li>
                            <li>This all-in-one design is compatible with all SIM card sizes: Nano, Micro and Standard</li>
                            <li>
                                Your phone should either be a Intouch America phone, a phone previously used with AT&T or Mobile, or an unlocked GSM phone.
                            </li>
                        </ul>
                        <div class="dsclmr">
                            <p>Prices are applied at time of purchase, and include FREE activation.</p>
                            <p>Devices are limited to stock on hand.</p>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="tpInr">
                        <div class="topCr">
                            <h2>CUSTOMER REVIEWS</h2>
                            <div class="rtngCnt">
                                <div class="rcStars">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </div>
                                <span>1 - 3 of 76 Reviews</span>
                                <a href="">Clear Filters</a>
                                <select class="selectpicker">
                                    <option>Rating</option>
                                    <option>Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="rwList">
                            <ul>
                                <li>
                                    <div class="rwltars">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        Couldn't be easier! AT&T coverage without the hassle, speaking to someone who speaks English. Best move I've ever made concerning cellular.
                                    </p>
                                    <div class="rwDtl">By Daniel O., CA, January 14, 2015</div>
                                </li>
                                <li>
                                    <div class="rwltars">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        Couldn't be easier! AT&T coverage without the hassle, speaking to someone who speaks English. Best move I've ever made concerning cellular.
                                    </p>
                                    <div class="rwDtl">By Daniel O., CA, January 14, 2015</div>
                                </li>
                                <li>
                                    <div class="rwltars">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        Couldn't be easier! AT&T coverage without the hassle, speaking to someone who speaks English. Best move I've ever made concerning cellular.
                                    </p>
                                    <div class="rwDtl">By Daniel O., CA, January 14, 2015</div>
                                </li>
                                <li>
                                    <div class="rwltars">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        Couldn't be easier! AT&T coverage without the hassle, speaking to someone who speaks English. Best move I've ever made concerning cellular.
                                    </p>
                                    <div class="rwDtl">By Daniel O., CA, January 14, 2015</div>
                                </li>
                                <li>
                                    <div class="rwltars">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        Couldn't be easier! AT&T coverage without the hassle, speaking to someone who speaks English. Best move I've ever made concerning cellular.
                                    </p>
                                    <div class="rwDtl">By Daniel O., CA, January 14, 2015</div>
                                </li>
                            </ul>
                            <div class="wrtRw clearfix">
                                <button class="btn btn-primary " type="button">
                                    Write A Review
                                </button>
                                <div class="rwlRght">
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination">
                                            <li>
                                                <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                          </a>
                                            </li>
                                            <li><a href="#">1</a></li>
                                            <li class="active"><span>2</span></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li>
                                                <a href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                          </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <div class="tpInr">
                        <div class="topCr">
                            <h2>HOW-TO VIDEOS &amp; MANUALS</h2>
                        </div>
                        <h3>EASY TO FOLLOW, STEP-BY-STEP VIDEO INSTRUCTIONS AND MANUALS FOR MASTERING YOUR DEVICE.</h3>
                        <div class="sprtLnkCnt clearfix">
                            <div class="leftCntsp">
                                <p>
                                    Intouch America offers how-to videos, manuals, and user guides to ensure you get the most value out of your device. The videos are entertaining, useful and convenient and, like the manuals, they're device-specific. You'll find the help you need quickly and easily.
                                </p>
                                <p>
                                    If you have a suggestion for a topic you'd like to see included in our videos, feel free to let us know.
                                </p>
                            </div>
                            <div class="sprtLinks">
                                <h3>SUPPORT LINKS</h3>
                                <p>No manuals currently available for the Intouch America SIM Card</p>
                            </div>
                        </div>
                        <h3>HOW-TO VIDEOS</h3>
                        <div class="htvcnt">
                            <a href="">
                            <img src="<?php echo get_template_directory_uri() ?>/img/hqdefault.jpg" alt="How To Video Image" />
                            <p>Using the Intouch America All-In-One SIM Card</p>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--New Added Content End Ravendra -->

<?php get_footer(); ?>